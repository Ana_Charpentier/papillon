from django.conf.urls import patterns, url
from polls import views

from polls.feeds import PollLatestEntries

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'create/$', views.create, name='create'),
    url(r'edit/(?P<admin_url>\w+)/$', views.edit, name='edit'),
    url(r'editChoicesAdmin/(?P<admin_url>\w+)/$', views.editChoicesAdmin, name='edit_choices_admin'),
    url(r'editChoicesUser/(?P<poll_url>\w+)/$',
        views.editChoicesUser, name='edit_choices_user'),
    url(r'category/(?P<category_id>\w+)/$', views.category, name='category'),
    url(r'poll/(?P<poll_url>\w+)/$', views.poll, name='poll'),
    url(r'poll/(?P<poll_url>\w+)/vote/$', views.poll, name='vote'),
    url(r'feeds/poll/(?P<poll_url>\w+)$', PollLatestEntries(), name='feed'),
)